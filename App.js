import React, {useEffect, useMemo, useRef, useState} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import ListItem from "./components/ListItem";
import { BottomSheetModal, BottomSheetModalProvider } from "@gorhom/bottom-sheet";
import Chart from "./components/Chart";
import {getMarketData} from "./services/cryptoService";


export default function App() {
    const [selectedCoinData, setSelectedCoinData] = useState(null);
    const [data, setData] = useState([]);

    useEffect(() => {
        const fetchMarketData = async () => {
            const marketData = await getMarketData();
            setData(marketData);
        }
        fetchMarketData();
    }, []);

    const bottomSheetModalRef = useRef(null);
    const snapPoints = useMemo(() => ['45%'], []);


    const openModal = (item) => {
        setSelectedCoinData(item);
        bottomSheetModalRef.current.present();
    }

    return (
        <BottomSheetModalProvider>
            <View style={styles.container}>
                <View style={styles.titleWrapper}>
                    <Text style={styles.largeTitle}>
                        Crypto Prices
                    </Text>
                </View>

                <View style={styles.divider} />

                <FlatList
                    keyExtractor={(item) => item.id}
                    data={data}
                    renderItem={({item}) => (
                        <ListItem
                            name={item.name}
                            symbol={item.symbol}
                            currentPrice={item.current_price}
                            percentage={item.price_change_percentage_7d_in_currency}
                            logoUrl={item.image}
                            onPress={() => openModal(item)}
                        />
                    )}
                />

            </View>

            <BottomSheetModal
                ref={bottomSheetModalRef}
                index={0}
                snapPoints={snapPoints}
                style={styles.bottomSheet}
            >
                {
                    selectedCoinData
                        ? (
                            <Chart
                                name={selectedCoinData.name}
                                symbol={selectedCoinData.symbol}
                                currentPrice={selectedCoinData.current_price}
                                percentage={selectedCoinData.price_change_percentage_7d_in_currency}
                                logoUrl={selectedCoinData.image}
                                sparkline={selectedCoinData.sparkline_in_7d.price}
                            />
                        )
                        : null
                }
            </BottomSheetModal>

        </BottomSheetModalProvider>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FAFAFA',
    },
    titleWrapper: {
        marginTop: 80,
        paddingHorizontal: 16,
        alignItems: 'center'
    },
    largeTitle: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    divider: {
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#A9ABB1',
        marginHorizontal: 16,
        marginVertical: 16
    },
    bottomSheet: {
        borderRadius: 15,
        shadowColor: '#000',
        elevation: 15
    }
});
