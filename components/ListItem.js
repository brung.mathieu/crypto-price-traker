import React from "react";
import {StyleSheet, TouchableOpacity, Image, View, Text, Platform} from "react-native";

const ListItem = ({ name, symbol, currentPrice, percentage, logoUrl, onPress }) => {

    const priceChangeColor = percentage > 0 ? '#34C759' : '#FF3B30';

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    const priceFormat = (price) => {
        return (
            Platform.OS === 'android'
            ? '$' + numberWithCommas(price.toFixed(2))
            : price.toLocaleString('en-US', {style: 'currency', currency: 'USD'})
        );
    }

    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.itemWrapper}>

                <View style={styles.leftWrapper}>
                    <Image
                        source={{ uri: logoUrl}}
                        style={styles.image}
                    />

                    <View style={styles.titlesWrapper}>
                        <Text style={styles.title}>{ name }</Text>
                        <Text style={styles.subtitle}>{ symbol }</Text>
                    </View>
                </View>

                <View style={styles.rightWrapper}>
                    <Text style={styles.title}>{ priceFormat(currentPrice) }</Text>
                    <Text style={[styles.subtitle, {color: priceChangeColor}]}>{ percentage.toFixed(2) }%</Text>
                </View>

            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    itemWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 16,
        paddingVertical: 10,
        marginVertical: 4,
        marginHorizontal: 5,
        backgroundColor: '#FFF',
        borderRadius: 12,
        shadowColor: '#000',
        elevation: 5
    },
    leftWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    image: {
        width: 48,
        height: 48,
    },
    titlesWrapper: {
        marginLeft: 12,
    },
    title: {
        fontSize: 18,
    },
    subtitle: {
        marginTop: 4,
        fontSize: 14,
        color: '#A9ABB1',
        textTransform: 'uppercase'
    },
    rightWrapper: {
        alignItems: 'flex-end'
    },
});

export default ListItem;