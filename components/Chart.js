import React, {useEffect} from "react";
import { StyleSheet, View, Text, Image, Platform, Dimensions } from 'react-native';
import 'react-native-reanimated';
import {
    ChartDot,
    ChartPath,
    ChartPathProvider,
    ChartYLabel
} from "@rainbow-me/animated-charts";
import {useSharedValue} from "react-native-reanimated";

export const {width: SIZE} = Dimensions.get('window');

const Chart = ({ name, symbol, currentPrice, percentage, logoUrl, sparkline }) => {

    const priceChangeColor = percentage > 0 ? '#34C759' : '#FF3B30';
    const latestCurrentPrice = useSharedValue(currentPrice);

    useEffect(() => {
        latestCurrentPrice.value = currentPrice;
    }, [currentPrice]);

    const numberWithCommas = (x) => {
        'worklet';
        return (
            x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        );
    }

    const priceFormat = (price) => {
        'worklet';
        price = parseFloat(price);
        return (
            Platform.OS === 'android'
                ? '$' + numberWithCommas(price.toFixed(2))
                : price.toLocaleString('en-US', {style: 'currency', currency: 'USD'})
        );
    };

    const formatUSD = value => {
        'worklet';
        if (value === '') {
            return `${priceFormat(latestCurrentPrice.value)}`;
        }
        return priceFormat(value);
    };


    return (
        <View style={styles.chartWrapper} >
            <ChartPathProvider data={{ points: sparkline, smoothingStrategy: 'bezier' }}>
                <View style={styles.titlesWrapper}>

                    <View style={styles.upperTitles}>
                        <View style={styles.upperLeftTitle}>
                            <Image source={{ uri: logoUrl }} style={styles.image} />
                            <Text style={styles.subtitle}>
                                { name } ({ symbol.toUpperCase() })
                            </Text>
                        </View>

                        <Text style={styles.subtitle}>7d</Text>
                    </View>

                    <View style={styles.lowerTitles}>
                        <ChartYLabel
                            format={formatUSD}
                            style={styles.boldTitle}
                        />

                        <Text style={[styles.title, {color: priceChangeColor}]}>
                            { percentage.toFixed(2) }%
                        </Text>
                    </View>
                </View>

                    <View style={styles.chartLineWrapper}>
                        <ChartPath height={SIZE / 2} stroke="black" width={SIZE} />
                        <ChartDot style={{ backgroundColor: 'black' }} />
                    </View>
                </ChartPathProvider>
        </View>

    )
}

const styles = StyleSheet.create({
    chartWrapper: {
        marginVertical: 16
    },
    titlesWrapper: {
        marginHorizontal: 16
    },
    upperTitles: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    upperLeftTitle: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    image: {
        width: 24,
        height: 24,
        marginRight: 4,
    },
    subtitle: {
        fontSize: 14,
        color: '#A9ABB1'
    },
    lowerTitles: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    boldTitle: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#000'
    },
    title: {
        fontSize: 18
    },
    chartLineWrapper: {
        marginTop: 40
    }
})

export default Chart;